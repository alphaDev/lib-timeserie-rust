use std::string::String;

use clap::Parser;

#[derive(Parser)]
#[clap(about, version, author)]
pub struct Args {
    #[clap(help = "path to the base file")]
    pub(crate) file_base: String,
}
