use bytes::BufMut;

pub struct Buffer {
    pub tags_hash: u64,
    pub start: u64,
}