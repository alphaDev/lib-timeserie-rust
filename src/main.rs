use std::collections::HashMap;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use clap::Parser;
use rocksdb::{DB, Options};

use chrono::Utc;
use once_cell::sync::Lazy;

use crate::args::Args;
use crate::buffer::Buffer;

mod buffer;
mod args;

// static GLOBAL_DATA: Lazy<String> = Lazy::new(|| Utc::now().to_string());
// static BUFFERS: Lazy<HashMap<u64, &Buffer>> = Lazy::new(|| HashMap::new());
//
// fn build_buffers() -> HashMap<u64, &Buffer> {
//     return HashMap::new();
// }




fn main() {
    let args: Args = Args::parse();
    println!("Hello, world!");
    println!("file_base {}", args.file_base);



    // NB: db is automatically closed at end of lifetime
    let path = args.file_base;
    {
       let db = DB::open_default(path).unwrap();
       db.put(b"my key", b"my value").unwrap();
       match db.get(b"my key") {
           Ok(Some(value)) => println!("retrieved value {}", String::from_utf8(value).unwrap()),
           Ok(None) => println!("value not found"),
           Err(e) => println!("operational problem encountered: {}", e),
       }
       db.delete(b"my key").unwrap();
    }
    let _ = DB::destroy(&Options::default(), path);

}

fn push_value(time: u64, value: u64, tags: &Vec<String>) {
    println!("push_value {}", calculate_hash(tags));
}

fn read_value(start: u64, end: u64, tags: &Vec<String>) {
    println!("read_value");
}

fn list_series() {
    println!("list_series");
}

fn delete(tags: &Vec<String>) {
    println!("delete");
}

fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}



